void setup() {
 size(600,300);
 // for smooth edges
smooth();
// draw rectangles with rect(topLeftX,topLeftY,width,height)
rect(10,10,100,100);
// draw ellipses with elli pse(centerX,centerY,diameterX,diameterY);
ellipse(60,200,100,100);

stroke(255,0,0);
rect(160,10,80,100);

// set the filling color of following shapes
fill(50,200,0);
ellipse(200,200,80,100);

noStroke(); // no outline
rect(280,10,120,40);

stroke(0);
noFill();
ellipse(340,200,120,40);
}

/*
Attention:
The parameters specify not necessarily the topLeft-corner/ center
and width,height  / radius. These are the default rectangle/ellipse
modes, but they can be changed:
http://processing.org/reference/rectMode_.html
http://processing.org/reference/ellipseMode_.html

Documentation

smooth:
http://processing.org/reference/smooth_.html
http://processing.org/reference/noSmooth_.html

rect:
http://processing.org/reference/rect_.html

ellipse:
http://processing.org/reference/ellipse_.html

fill:
http://processing.org/reference/fill_.html

noStroke:
http://processing.org/reference/noStroke_.html

noFill:
http://processing.org/reference/noFill_.html

rectMode:
http://processing.org/reference/rectMode_.html

ellipseMode:
http://processing.org/reference/ellipseMode_.html

http://processing.org/tutorials/color/
http://processing.org/tutorials/drawing/

*/
