void setup() {
  size(600, 600); 
  noStroke();
  colorMode(HSB);
  randomAngle();
  randomFillColor();
  randomDiameter();
  background(0);
}


float angle, dist;
float diameter, hue;
float x, y;
float angleInc= 0.1f;
float distInc = 1;
int colorChange = int(random(1, 20));
int ellipsesPerFrame = int ( random(5, 50));

void draw() {
  for (int i=0; i <ellipsesPerFrame;i++) {
    x = cos(angle)*dist;
    x+= width/2;
    y = sin(angle)*dist;
    y += height/2;
    ellipse(x, y, diameter, diameter);

    checkReset();

    angle += angleInc;
    dist += distInc;
    hue = (hue + colorChange) % 256;
    fill(hue, 256, 256, 50);
  }
}

void randomAngle() {
  angle = random(TWO_PI);
}

void randomFillColor() {
  hue =random(256);
  fill(hue, 256, 256, 50);
}

void randomDiameter() {
  diameter = random(15, 30);
}

void checkReset() {
  if (x < 0 || x > width || y < 0 || y > height) {
    dist = 0;
    randomAngle();
    randomDiameter();
    colorChange = int(random(1, 20));
    ellipsesPerFrame = int ( random(5, 50));
  }
}

