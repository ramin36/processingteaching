void setup() {
  size(1024, 400);
  colorMode(HSB);
  background(255);

  // a for loop, repeats the statements in its body until the condition is not fullfilled anymore
  /* there are three parts in the for-loop head
   1. initialisation: initialize a variable 
   int i=0  
   2. condition: repeat the loop body as long as the condition is fullfilled
   i < 10
   3. incrementation: change the initialized variable after each iteration, in order to count the iterations
   i += 1
   */
  for (int i =0; i< 10; i+= 1) {
    println(i);
  }

  // first rainbow with full saturation,brightness
  for (int i = 0; i < 256; i++) { 
    stroke(i, 255, 255);
    line(i, 0, i, height);
  }

  // second rainbow with half  saturation
  int nextRainbowX = 256;
  for (int i = 0; i < 256; i++) {
    stroke(i, 126, 255);
    line(i+nextRainbowX, 0, i+nextRainbowX, height);
  }

  // third rainbow with half brightness
  nextRainbowX += 256;
  for (int i = 0; i < 256; i++) {
    stroke(i, 255, 126);
    line(i+nextRainbowX, 0, i+nextRainbowX, height);
  }

  // forth rainbow with half alpha ( it looks like the second, but change the background to see the difference
  nextRainbowX += 256;
  for (int i = 0; i < 256; i++) {
    stroke(i, 255, 255, 126);
    line(i+nextRainbowX, 0, i+nextRainbowX, height);
  }
}

/*
Reference:
for-loops:
http://processing.org/reference/for.html
*/
