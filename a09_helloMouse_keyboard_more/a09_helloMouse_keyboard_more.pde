/*
This is a more advanced example of mouse and key interaction
 The mousePressed method makes a distiction between left and right mouse button.
 Now we check if the right mouse button is pressed in the draw.
 That means, we constantly do an action, when the button is pressed.
 We pull the rect towards the mouse.
 
 we randomly change the filling color when we press ENTER
 
 */

void setup() {
  size(600, 600); 
  noStroke();
  rectMode(CENTER);
  // A method we defined at the bottom to make a random fill color
  randomFillColor();
}

// x and y coordinates of the rect
float rectX, rectY;
// rect size. used to set the width & height
float rectSize = 30;

void draw() {
  background(0);
  rect(rectX, rectY, rectSize, rectSize);
  // check if the right mousebutton is pressed
  if (mousePressed && mouseButton== RIGHT) {
    moveRectTowardsMouse();
  }  // if not move it like before
  else {
    rectY += 5;
  }
  if (rectY>height) {
    rectY =0;
  }
}

void mousePressed() {
  if (mouseButton==LEFT) {
    rectX = mouseX;
    rectY = mouseY;
  }
}

void keyPressed() {
  //println("key pressed");
  // check what key we pressed
  if (key=='c') {
    rectY = 0;
  }
  else if (key=='a') {
    rectSize-= 5;
    if (rectSize == 0) {
      rectSize = 5;
    }
    // this could have also be done using the max function
    // max(float a,float b) returns the bigger of the two passed values a & b.
    rectSize = max(rectSize-5, 5);
  }
  else if (key=='s') {
    rectSize = min(rectSize+5, 100);
  } // get the keyCode of the key we pressed. This is how we check on special keys
  else if (keyCode == ENTER) { // ENTER is a constant
    randomFillColor();
  }
}

// the speed, with which the rect moves towards the mouse
int moveTowardsMouseSpeed = 3;

void moveRectTowardsMouse() {
  float xDist = mouseX-rectX;
  if (abs(xDist) < moveTowardsMouseSpeed) {
    rectX = mouseX;
  } 
  else {
    float dirX = sgn(xDist);
    rectX+= dirX*moveTowardsMouseSpeed;
  }
  float yDist = mouseY-rectY;
  if (abs(yDist) < moveTowardsMouseSpeed) {
    rectY = mouseY;
  } 
  else {
    float dirY = sgn(yDist);
    rectY+= dirY*moveTowardsMouseSpeed;
  }
}  

/* the mathematical signum function. sgn for all positive numbers is 1
 for all negative number -1, and for 0 0. */
float sgn(float num) {
  return num / abs(num);
}


void randomFillColor() {
  // each part of the color is random number between 0 and 255 (not 256!) 
  fill(random(256), random(256), random(256));
}

/*
 
 Reference:
 
 keyCode:
 http://processing.org/reference/keyCode.html
 
 absolute:
 http://processing.org/reference/abs_.html
 
 random:
 http://processing.org/reference/random_.html
 
 
 */
