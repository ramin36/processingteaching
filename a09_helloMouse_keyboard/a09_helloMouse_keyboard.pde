void setup() {
  size(600, 600); 
  noStroke();
  rectMode(CENTER);
}

// x and y coordinates of the rect
float rectX, rectY;
// rect size. used to set the width & height
float rectSize = 30;


void draw() {
  background(0);
  rect(rectX, rectY, rectSize, rectSize);
  rectY += 5;
  if (rectY>height)
    rectY =0;
}

// this method is called when the mouse is pressd
void mousePressed() {
  rectX = mouseX;
  rectY = mouseY;
}

// this method is called when a key is pressed
void keyPressed() {
  //println("key pressed");
  // check what key we pressed
  if (key=='c') { // maybe c?
    rectY = 0; // okay bring it to the top
  }
  else if (key=='a') { // if not then maybe a?
    rectSize-= 5;  // scale the rect down
    if (rectSize == 0) {
      rectSize = 5;
    }
    // this could have also be done using the max function
    // max(float a,float b) returns the bigger of the two passed values a & b.
    rectSize = max(rectSize-5, 5);
  }
  else if (key=='s') { // s?
    rectSize = min(rectSize+5, 100); // enlarge the rect
  }
}

/*
Reference:
 
 mouse pressed:
 http://processing.org/reference/mousePressed.html
 http://processing.org/reference/mouseX.html
 http://processing.org/reference/mouseY.html
 
 key pressed:
 http://processing.org/reference/keyPressed_.html
 http://processing.org/reference/key.html
 
 
 there is more, check the processing reference
 
 mouseClicked(), mouseDragged(), mouseMoved(), 
 mousePressed(), mouseReleased() mouseWheel(), ...
 
 keyPressed(), keyReleased() keyTyped()
 
 
 min: 
 http://processing.org/reference/min_.html
 
 max:
 http://processing.org/reference/max_.html
 
 */
