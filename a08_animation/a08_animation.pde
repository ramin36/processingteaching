void setup() {
  size(600, 600); 
  noStroke();
  rectMode(CENTER);
}

// y coordinate of the recangle
float rectY;

void draw() {
  background(0);
  // draw the rect at the actual height
  rect(100, rectY, 30, 30);
  // move the rect down a little bit
  rectY += 5; // short for rectY = rectY + 5;
  // bring it to the top if it is out of the window
  if (rectY>height)
    rectY =0;
  // this could also be done with % (modulo)
  // Calculates the remainder when one number is divided by another
  //rectY = rectY % height; 
  // or
  // rectY %= height;
}

/*
Reference:
 % (modulo)
 http://processing.org/reference/modulo.html
 
 if
 http://processing.org/reference/if.html
 */
