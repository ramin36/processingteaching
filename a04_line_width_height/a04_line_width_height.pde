void setup() {
  size(600, 400);
  background(255);
  // drawing a point
  point(10,10);
  
  // drawing a line 
  line(0, 0, 100, 200); 
  /* 0,0 is top,left corner 600,400 bottom,right of the window
   a point in the frame is called coordinate
   a point has a x-coordinate and a y-coordinate
   line(x1, y1, x2, y2)
   */
}

void draw() {

  // background(255);
  // line(0,0,frameCount,200); 
  /* frameCount is a variable that processing holds
   it gives you the number of the actual frame */

  // line(0,0,width,height);
  /* two other available variables are width and height, which give
   you the dimensions of the window */
   
   // remove the background call!
}

/*
Reference

Point:
// http://processing.org/reference/point_.html

Line:
http://processing.org/reference/line_.html


Tutorial Coordinate System:
http://processing.org/tutorials/drawing/
