void setup() {
size(600,400);

// arc: a piece of the cake
// arc(a, b, c, d, start, stop)
// start and stop parameters to specify the angles (in radians, from 0 - TWO_PI)
arc(50, 55, 50, 50, 0, HALF_PI);
noFill();
arc(50, 55, 60, 60, HALF_PI, PI);

// quad: arbitrary four sided polygon
// quad(x1, y1, x2, y2, x3, y3, x4, y4)
quad(138, 31, 186, 20, 169, 63, 130, 76);

// trianles
// triangle(x1, y1, x2, y2, x3, y3)
fill(100);
triangle(200,30,280,40,220,50);

beginShape();
vertex(20,200);
vertex(50,180);
vertex(80,200);
vertex(80,250);
vertex(20,250);
endShape(CLOSE);

/* documentation
arc:
http://processing.org/reference/arc_.html

tutorial on radians and angles:
http://processing.org/tutorials/trig/

quad:
http://processing.org/reference/quad_.html

triangles:
http://processing.org/reference/triangle_.html

shapes:
http://processing.org/reference/beginShape_.html

*/

}
