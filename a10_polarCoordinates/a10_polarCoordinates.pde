void setup() {
  size(600, 600);
  background(255);
  smooth();
  
  // the center of the window
  float x_center = width/2;
  float y_center = height/2;
  ellipse(x_center, y_center, 30, 30);
  
  int numberOfLines = 12;
  // step increase: angle difference between two lines
  int stepInc = 360 / numberOfLines ;
  for (int i=0; i < 360; i+=stepInc) { // consider i+=stepInc
    float angleRad = radians(i); // from degree(0-360) to radians(0-TWO_PI)
    println("i:"+i);
    // calculate a point with the angle and distance 100 relative to the center
    float x= x_center + cos(angleRad)*100;
    float y= y_center + sin(angleRad)*100;
    line(x_center, y_center, x, y);
  }
}

/*

Reference:
radians:
http://processing.org/reference/radians_.html

cos:
http://processing.org/reference/cos_.html

sin:
http://processing.org/reference/sin_.html

Also checkout: !!!
http://processing.org/tutorials/trig/
*/
