// I AM A COMMENT, I WILL BE IGNORED BY THE COMPILER
// AND NOT BE EXECUTED.


/*
 I am a multy line comment. Everything will be ignored until you close me
 Lets's have some more lines...
 ...
 ciao
*/

void setup() {
 size(600,400); // set the window size, width=600, height=400
 background(0); // clear the screen with black
}

/*

Reference:

size:
http://processing.org/reference/size_.html

background:
http://processing.org/reference/background_.html
