/*
A colorful rectangle is falling from the sky.
When it disappears in the ground we create a new one
*/

void setup() {
  size(600, 600); 
  noStroke();
  rectMode(CENTER);
  colorMode(HSB);
  setRandomRect();
}

float rectX, rectY, rectWidth, rectHeight, speed;

void draw() {
  background(0);
  rect(rectX, rectY, rectWidth, rectHeight);
  rectY += speed;
  // rectY is in the center of the rect
  // the top of the rect is rectHeight/2 above 
  if (rectY-rectHeight/2>height)
    setRandomRect();
}

void setRandomRect() {
  rectX = random(width);
  rectWidth = random(20, 60);
  rectHeight = random(20, 60);
  rectY = - rectHeight/2;
  fill((int) random(256), 255, 255);
  speed = random(2, 12);
}


